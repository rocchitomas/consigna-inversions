Dado("que mi cuit es {string}") do |cuit|
  post '/reset'
  @cuit = cuit
end

Dado("que compro $ {int} dólares a cotizacion $ {float}") do |capital_invertido, cotizacion_que_compre_dolar|
  @capital_invertido = capital_invertido
  @cotizacion_que_compre_dolar = cotizacion_que_compre_dolar
end

Dado("que el interes de plazo fijo es {int} % anual") do |interes_anual|
  @interes_anual = interes_anual
end

Dado("que compro $ {float} en acciones a $ {float} por acción") do |capital_invertido, valor_por_accion_comprada|
  @capital_invertido = capital_invertido
  @valor_por_accion_comprada = valor_por_accion_comprada
end

Cuando("vendo esos dolares a cotización $ {float}") do |cotizacion_que_vendi_dolar|
  body = {"capital": @capital_invertido, "cotizacion_que_compre_dolar": @cotizacion_que_compre_dolar, "cotizacion_que_vendi_dolar": cotizacion_que_vendi_dolar }
  post "/inversiones/#{@cuit}/dolares", body.to_json, "CONTENT_TYPE" => "application/json"
  expect(last_response.status).to be == 200

  get "/inversiones/#{@cuit}"
  expect(last_response.status).to be == 200
  @json_response = JSON.parse(last_response.body)
end

Cuando("invierto $ {float} en un plazo fijo a {int} dias") do |capital_invertido, plazo_en_dias|
  body = {"capital": capital_invertido, "interes_anual": @interes_anual, "plazo_en_dias": plazo_en_dias}
  post "/inversiones/#{@cuit}/plazo-fijos", body.to_json, "CONTENT_TYPE" => "application/json"
  expect(last_response.status).to be == 200

  get "/inversiones/#{@cuit}"
  expect(last_response.status).to be == 200
  @json_response = JSON.parse(last_response.body)
end

Cuando("vendo esas acciones a $ {float} por acción") do |valor_por_accion_vendida|
  body = {"capital": @capital_invertido, "valor_por_accion_comprada":@valor_por_accion_comprada, "valor_por_accion_vendida":valor_por_accion_vendida }
  post "/inversiones/#{@cuit}/acciones", body.to_json, "CONTENT_TYPE" => "application/json"
  expect(last_response.status).to be == 200

  get "/inversiones/#{@cuit}"
  expect(last_response.status).to be == 200
  @json_response = JSON.parse(last_response.body)
end

Entonces("obtengo una ganancia bruta de $ {float}") do |monto_ganancia_bruta_esperada|
  ganancia_bruta = @json_response["ganancia_bruta"].to_f
  expect(ganancia_bruta).to eq monto_ganancia_bruta_esperada
end

Entonces("$ {float} de impuestos-comisiones") do |monto_impuestos_esperado|
  impuesto = @json_response["impuestos"].to_f
  expect(impuesto).to eq monto_impuestos_esperado
end

Entonces("eso resulta en una ganancia neta de $ {float}") do |monto_ganancia_neta_esperada|
  ganancia_neta = @json_response["ganancia_neta"].to_f
  expect(ganancia_neta).to eq monto_ganancia_neta_esperada
end